\section{Základy statistické indukce, náhodný výběr, bodové odhady pro střední hodnotu a rozptyl, intervalové odhady pro střední hodnotu, testování statistických hypotéz o střední hodnotě.	}
Na základě skutečných výsledků vybíráme vhodný model, odhadujeme hodnoty jeho parametrů, testujeme hypotézy o těchto parametrech a ověřujeme shodu modelu se skutečností.
\begin{definition}
	$n$-tice stejně rozdělených nezávislých náhodných veličin (i.i.d.) $X_1, \dots, X_n$ s distribuční funkcí $F$ se nazývá náhodný výběr rozdělení $F$. Realizace náhodného výběru (vektor náhodných pozorování) je $n$-tice kontrétních pozorovaných čísel $x_1, \dots, x_n$.
\end{definition}
Na základě náhodného výbéru můžeme uskutečnit statistický výzkum (indukci), který má 3 základní kroky:
\begin{itemize}
	\item \textbf{Odhad parametrů rozdělení} -- zúžení úvah na třídu rozdělení $F_\theta$ určenou neznámým parametrem $\theta$. Vychází z apriorní znalosti, intuice nebo zkušenosti.
		
	\item \textbf{Odhad parametů rozdělení}
	
	
		Dá se realizovat dvěma způsoby:
			\begin{itemize}
				\item Bodový odhad -- určení nejpravděpodobnější hodnoty $\theta$.
				\item Invervalový odhad -- určení intervalu, ve kterém se s nějakou pravděpodobností nachází $\theta$.
			\end{itemize}
	\item \textbf{Ověření správnosti modelu}
	
		Testování hypotéz:
			\begin{itemize}
				\item Parametrické testy -- vytvoříme hypotézu o parametru $\theta$ (např. $\theta = 0$) a na základě dat se rozhodujeme, zda tuto hypotézu můžeme zamítnout.
				\item Testy dobré shody -- ověřujeme hypotézy o tvaru pravděpodobnostního rozdělení. Například jestli má zkoumaná veličina normální rozdělení.
			\end{itemize}
\end{itemize}
\subsection{Bodové odhady}

\begin{definition}
	Bodový odhad parametru $\theta$ je funkce $\hat{\theta}_n(X_1, \dots, X_n)$ náhodného výběru, která nezávisí na hodnotách $\theta$.
	Nejpoužívanější bodové odhady:
	\begin{itemize}
		\item \emph{Výběrový průměr}: bodový odhad střední hodnoty:
		$$
		\bar{X}_n = \frac{1}{n} \sum_{i=1}^{n} X_i
		$$
		\item \emph{Výběrový rozptyl}: bodový odhad rozptylu:
		$$
		s_n^2 = \frac{1}{n-1}\sum_{i=1}^n (X_i - \bar{X}_n)^2
		$$
		\item \emph{Výběrová směrodatná odchylka}: bodový odhad směrodatné odchylky:
		$$
		s_n = \sqrt{s_n^2}
		$$
	\end{itemize}
\end{definition}

\begin{definition}
	Odhad $\hat{\theta}_n$ parametru $\theta$ se nazýva \emph{nestranný} (nevychýlený), jestliže
	$$
	E\hat{\theta}_n(X_1, \ldots, X_n) = \theta, \qquad \text{pre všetky}\ \theta \in \Theta
	$$
	Nestranný odhad se též nazýva \emph{nejlepší}, jestliže pro každý jiný nestranný odhad $\hat{\theta}_n^0$ platí:
	$$
	\text{var}(\hat{\theta}_n^0) \geq \text{var}(\hat{\theta}_n) \qquad \text{pro všechny}\ \theta \in \Theta
	$$
\end{definition}

\begin{definition}
	Odhad $\hat{\theta}_n$ parametru $\theta$ sa nazýva \emph{konzistentní}, jestliže pro každé $\epsilon > 0$ a $\theta \in \Theta$ platí:
	$$
	\lim_{n \rightarrow +\infty} P(|\hat{\theta}_n(X_1, \ldots, X_n) - \theta| \geq \epsilon) = 0
	$$
	
	Konzistence znamená, že volbou dostatečně velkého $n$ lze učinit chybu dostatečně malou.
\end{definition}
\begin{definition}
	Nechť $X_1, \dots, X_n$ je náhodný výběr s konečnými momenty $\mu_k = EX^k$ (pro $k \leq K$). Předpokládejme, že odhadovaný parametr $\theta$ je funkcí momentů:
	$$
	\theta = H(\mu_1, \ldots, \mu_K)
	$$
	Odhad parametru $\theta$ \emph{momentovou metodou} je:
	$$
	\hat{\theta} = H(m_1, \ldots, m_K) \qquad \text{kde} \qquad m_k = \frac{1}{n} \sum_{i=1}^n X_i^k
	$$
	jsou odhady momentů.
\end{definition}
Konzistentní odhady s dobrymi vlastnostmi je možné získat metodou maximální věrohodnosti. Cílem je \textbf{maximalizovat} věrohodnostní funkci pro dané naměřené hodnoty.
\begin{definition}
	Nechť $\mathbf{X} = (X_1, \ldots, X_n)$ je náhodný vektor pozorovaní se sdruženou:
	$$
	\text{hustotou } f_\theta(\mathbf{x}) = \prod_{i=1}^n f_\theta(x_i) \qquad \text{\emph{(pro spojité rozdělení.)}}
	$$
	nebo
	$$
	\text{pravděpodobností } p_\theta(\mathbf{x}) = \prod_{i=1}^n P_\theta(X_i = x_i) \qquad \text{\emph{(pro diskrétní rozdělení.)}}
	$$
	Při pevných hodnotách pozorovaných dat $x = (x_1, \dots , x_n)$ se $f_\theta(x)$ resp. $p_\theta(x)$ jakožto
	funkce $\theta$ nazývá \textbf{věrohodnostní funkcí} a značí se $L(\theta; x)$, případně jen $L(\theta)$.
\end{definition}
\begin{definition}
	Hodnotu $\hat{\theta}$ parametru $\theta$, která maximalizuje věrohodnostní funkci $L(\theta; x)$ pro dané $X = x$, se nazývá maximalně věrohodný odhad (MLE) parametru $\theta$. To znamená, že
	$$ L(\hat{\theta}_n, x) \geq L(\theta, x) \quad \text{pro všechna } \theta \in \Theta.$$
\end{definition}
\textcolor{red}{Možná nějakej příklad.}

\subsection{Intervalové odhady}
\begin{definition}
	Nechť $X_1, \dots, X_n$ je náhodný výběr z rozdělení určeného parametrem $\theta$. Interval $(L,U)$ s krajními body určenými statistikami $ L \equiv L(X) \equiv L(X_1, \dots, X_n)$ a $U \equiv U(X) \equiv U(X_1, \dots, X_n)$ splňující
	$$ P(L < \theta < U) = 1- \alpha $$
	se nazývá $100 \cdot (1 - \alpha)\%$ \textbf{interval spolehlivosti}, případně \textbf{konfidenční interval}. 
	
	Statistika $L$ resp. $U$ se nazývá \textbf{dolní} resp. \textbf{horní} mez intervalu spolehlivosti. Číslo $(1-\alpha)$ se nazývá \textbf{hladina spolehlivosti}.
\end{definition}

\begin{definition}
	Pokud konstruujeme statistiky tak, aby
	$$
	P(L \leq \theta) = 1 - \alpha \qquad \text{resp.} \qquad P(\theta \leq U) = 1 - \alpha
	$$
		tak interval $(-\infty, U)$ resp. $(L, +\infty)$ nazývame \textbf{dolní} resp. \textbf{horní interval spolehlivosti}.
\end{definition}
\subsubsection{Intervaly spolehlivosti pro střední hodnotu}
Při \textbf{známém rozptylu} $\sigma^2$.
\begin{definition}
	Uvažujme náhodný výběr $X_1, \dots , X_n$ z normálního rozdělení $N(\mu, \sigma^2)$	a předpokládejme, že známe rozptyl $\sigma^2$. Oboustranný $100 \cdot (1 - \alpha)\%$ interval spolehlivosti pro $\mu$ je
	$$ \left( \bar{X}_n - z_{\alpha/2} \frac{\sigma}{\sqrt{n}}, \bar{X}_n + z_{\alpha/2} \frac{\sigma}{\sqrt{n}}\right)  $$
	kde $z_{\alpha/2}=\Phi^{-1}(1-\alpha/2)$ je \textbf{kritická hodnota} standardního normálního rozdělení, tj. číslo, pro které platí $P(Z > z_{\alpha/2} = \alpha/2$, jestliže $ Z \sim N(0,1)$.
	
	\noindent
	Jednostranné $100\cdot (1-\alpha)\%$ intervaly spolehlivosti pro $\mu$ jsou pak
	$$ \left( \bar{X}_n - z_{\alpha} \frac{\sigma}{\sqrt{n}}, +\infty \right) \text{ a } \left( -\infty, \bar{X}_n + z_{\alpha} \frac{\sigma}{\sqrt{n}} \right)  $$
	při stejném značení.
\end{definition}

Při \textbf{neznámém rozptylu} $\sigma^2$ aproximujeme hodnotu $\sigma$ odmocninou z výběrového rozptylu a použijeme studentovo rozdělení.
\begin{definition}
	Uvažujme náhodný výběr $X_1, \dots , X_n$ z normálního rozdělení $N(\mu, \sigma^2)$.. Oboustranný $100 \cdot (1 - \alpha)\%$ interval spolehlivosti pro $\mu$ je
	$$ \left( \bar{X}_n - t_{\alpha/2,n-1} \frac{s_n}{\sqrt{n}}, \bar{X}_n + t_{\alpha/2,n-1} \frac{s_n}{\sqrt{n}}\right)  $$
	kde $t_{\alpha/2,n-1}$ je \textbf{kritická hodnota} studentova t-rozdělení s $n-1$ stupni volnosti.
	
	\noindent
	Jednostranné $100\cdot (1-\alpha)\%$ intervaly spolehlivosti pro $\mu$ jsou pak
	$$ \left( \bar{X}_n - t_{\alpha,n-1} \frac{s_n}{\sqrt{n}}, +\infty \right) \text{ a } \left( -\infty, \bar{X}_n + t_{\alpha,n-1} \frac{s_n}{\sqrt{n}} \right)  $$
	při stejném značení.
\end{definition}
Konfidenční intervaly pro rozptyl $\sigma^2$ získáme postupem založeným na chi-kvadrát rozdělení $X^2_{n-1}$.
\subsection{Testování hypotéz}
Mějme náhodný výběr z nějakého rozdělení. Tvrzení o tomto rozdělení, která nemůžeme jednoznačně potvrdit nazýváme \textbf{hypotézy}.

Používáme dva základní pojmy:
\begin{itemize}
	\item \textbf{Nulová hypotéza} $H_0$ označuje tvrzení, které chceme potvrdit nebo zamítnout.
	\item \textbf{Alternativní hypotéza} $H_A$ je opačné tvrzení, které stavíme proti $H_0$.
\end{itemize}
Typy hypotéz:
\begin{itemize}
	\item \textbf{Neparametrické} -- výběr z obecného rozdělení. Tvrzení se týkají různých vlastností rozdělení (např. mediánu), případně tvaru celého rozdělení (testy dobré shody).
	\item \textbf{Parametrické} -- náhodný výběr z rozdělení s parametrem $\theta \in \mathbb{R}^d$. Tvrzení se týkají hodnoty $\theta$.
\end{itemize}
Při rozhodování se můžeme dopustit jedne ze dvou chyb:
\begin{itemize}
	\item Zamítneme $H_0$, ačkoliv platí -- \textbf{chyba prvního druhu}.
	\item Nezamítneme $H_0$, ačkoliv neplatí (platí $H_A$) -- \textbf{chyba druhého druhu}.
\end{itemize}
Chceme, aby pravděpodobnost chyby prvního druhu byla nejvýš rovna zvolené hodnotě $\alpha$, kterou \textbf{nazýváme významností testu}. Běžně volíme $\alpha = 5\%$ nebo $\alpha = 1\%$


Průběh testu:
\begin{itemize}
	\item Zvolíme hladinu významnosti $\alpha$.
	\item Napozorujeme realizace náhodného výběru.
	\item Zkonstruujeme konfidenční interval o spolehlivosti $1-\alpha$, odpovídající alternativní hypotéze $H_A$.
	\item Zamítneme $H_0$, pokud testovaná hodnota $\theta_0$ neleží v tomto intervalu spolehlivosti.
\end{itemize}
Pokud máme jako hypotézu nerovnost ($\not=$), používáme oboustranný interval, oproti tomu při $H_A$ vyjádřenou nerovností ($<,>$) používáme příslušný jednostranný interval.

\textbf{Příklad}: Bezpečnostní senzor pravidelně kontroluje PC učebnu. Pokud v učebně není detekovaný pohyb, senzor vrací signál $x=w$, kde $w$ je náhodná veličina se střední hodnotou $0$ a malým rozptylem. V případě detekce pohybu vrací senzor signál $ x = w + \Theta$, kde $\Theta$ je nějaká kladná neznámá konstanta.

Po 35 pozorováních jsme určili konfidenční intervaly pro $\mu = EX$ jako:
\begin{itemize}
	\item $90\%$ konfidenční interval $(0.405,5.394)$
	\item $95\%$ konfidenční interval $(0.072,5.872)$
\end{itemize}
Chceme otestovat přítomnost pohybu při chybě $5\%$ a $2.5\%$, pro hypotézy:
\begin{itemize}
	\item $H_0$ -- žádný pohyb, $\mu=0$
	\item $H_A$ -- pohyb, $\mu>0$
\end{itemize} 
Výsledek:
\begin{itemize}
	\item $5\%$ -- $0 \not\in (0.405,+\infty)$ zamítáme $H_0$, detekovaný pohyb.
	\item $2.5\%$ -- $0 \in (-0.072,+\infty)$ nezamítáme $H_0$, nevíme, zda je detekovaný pohyb.
\end{itemize}
