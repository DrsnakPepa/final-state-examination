\section{Metody řešení rekurentních rovnic, sestavování a řešení rekurentních rovnic při analýze časové složitosti algoritmů.}
Rekurentní problém je problém, jehož řešení je závislé na řešení menších instancí stejného problému.
Řešení lze obvykle vyjádřit rekurzivním algoritmem a jeho vlastnosti odpovídajícími rekurentními vztahy (rekurencemi).
Příklad: Víme, že $T1 = 1, T2 = 3$ (můžeme doplnit i $T_0 = 0$), z popsaného postupu řešení plyne

$$ T_n = \begin{cases}
0 \text{ pro }n = 0,\\
2\cdot T_{n−1} + 1 \text{ pro } n > 0.
\end{cases} 
$$
Jak určíme např. $T_8$?

\begin{align*}
T_2 & = 3, T_3 = 2 \cdot T_2 + 1 = 2 \cdot 3 + 1 = 7,\\
T_4 & = 2 \cdot T_3 + 1 = 2 \cdot 7 + 1 = 15,\\
T_5 & = 2 \cdot T_4 + 1 = 2 \cdot 15 + 1 = 31,\\
T_6 & = 2 \cdot T_5 + 1 = 2 \cdot 31 + 1 = 63,\\
T_7 & = 2 \cdot T_6 + 1 = 2 \cdot 63 + 1 = 127,\\
T_8 & = 2 \cdot T_7 + 1 = 2 \cdot 127 + 1 = 255.
\end{align*}


Zkusíme uhádnout vyjádření $T_n$ v uzavřeném tvaru:
$$T_n = 2^n - 1, pro n \geq 0. $$
To odpovídá prvním spočteným hodnotám, je to ale opravdu správně?
Jak dokážeme správnost vztahu?

\begin{definition}
	\textbf{Lineární rekurentní rovnice} řádu $k \in N $je libovolná rovnice ve tvaru
	$$ a_{n+k} + c_{k−1}(n)a_{n+k−1} + \dots + c_1(n)a_{n+1} + c_0(n)a_n = b_n$$
	pro všechna $n \geq n_0$, kde $n_0 \in Z$, $c_i(n)$ pro $i = 0, \dots , k − 1$ (tzv. koeficienty rovnice) jsou nějaké funkce $Z \rightarrow R$, přičemž $c_0(n)$ není identicky nulová funkce, a $\{b_n\}_{n=n_0}^\infty$ (tzv. pravá strana rovnice) je pevně zvolená posloupnost reálných čísel.

	
	\noindent
	Jestliže $b_n = 0$ pro všechna $n \geq n_0$, pak se příslušná rovnice nazývá homogenní.
\end{definition}
\subsection{Metody řešení rekurentních rovnic}
\subsubsection*{Substituční metoda}


\begin{itemize}
	\item Odhadneme (uhádneme) tvar řešení (=indukční hypotéza).
	\item Pomocí matematické indukce nalezneme konstanty a ověříme
	správnosti odhadnutého řešení.

\end{itemize}
Tuto metodu lze použít pro určení horní i dolní meze řešené funkce.
Příklad: Uvažujme rovnici
\begin{equation} \label{eq:priklad1}
	t(n) = 2t(\lfloor n/2 \rfloor ) + n
\end{equation}
Jako \textbf{horní} odhad řešení zkusme
\begin{equation} \label{eq:priklad2}
	t(n) \leq cn \log n, \text{ kde } c > 0\text{ je vhodně zvolená konstanta.}
\end{equation}
Indukcí dokažme správnost odhadu. Tedy, že pro řešení rovnice platí
$$
t(n) = O(n \log n).
$$
\textbf{Indukční krok}
(ověření, že $t(n) \leq cn \log n$ vyhovuje rekurenci $t(n) = 2t(\lfloor n/2 \rfloor) + n)$


Předpokládejme, že  \eqref{eq:priklad2} platí pro $\lfloor n/2 \rfloor$ a dosaďme $t(\lfloor n/2 \rfloor) \leq c \lfloor n/2\rfloor \log \lfloor n/2 \rfloor$ do \eqref{eq:priklad1}. Dostaneme

\begin{align*}
t(n) &\leq 2(c \lfloor n/2 \rfloor \log \lfloor n/2\rfloor) + n\\
&= cn \log(n/2) + n\\
&= cn \log n - cn \log 2 + n\\
&= cn \log n - cn + n\\
&\leq cn \log n,\quad \text{ pokud }c \geq 1
\end{align*}


Indukční základ:
Je třeba dokázat, že existuje konstanta $c \geq 1$ taková, že \eqref{eq:priklad2} přímo platí pro počáteční hodnoty $n$.

\begin{itemize}
	\item Pro jednoduchost předpokládejme, že platí $t(1) = 1$. (Lze zvolit	jakoukoli jinou konstantu.) Pak z \eqref{eq:priklad1} plyne $t(2) = 4$ a $t(3) = 5$.
	\item Pak ale \eqref{eq:priklad2} neplatí pro $n = 1$, nebo neexistuje konstanta $c$ taková,
	že $t(1) \leq  c1 log 1 = 0$.
	\item Naštěstí řešíme \eqref{eq:priklad1} asymptoticky, takže stačí ukázat, že \eqref{eq:priklad2} platí od
	nějakého $n_0 \geq 1$.
	\item Stačí proto ukázat, že lze zvolit dostatečně velkou konstantu $c$, aby platilo $t(2) = 4 \leq c 2 \log 2$ nebo $t(3) = 5 \leq c 3 \log 3$.
	\item Triviálně lze spočítat, že stačí zvolit $c \geq 2$.

\end{itemize}

Naprosto analogickým způsobem lze ukázat, že pro řešení rovnice \eqref{eq:priklad1} platí
\begin{equation}\label{eq:priklad3}
t(n) \geq cn \log n,\text{ kde } c > 0\text{ je vhodně zvolená konstanta.}
\end{equation}

\begin{align*}
t(n) &\geq 2(c \lfloor n/2 \rfloor \log \lfloor n/2\rfloor) + n \\
&\geq cn \log(n/2) + n \\
&= cn \log n - cn \log 2 + n \\
&= cn \log n + n(1 − c) \\
&\geq cn \log n,\text{ pokud }c \leq 1.\\
\end{align*}
Z indukčního základu odvodíme např. $c = 0,5$.
\textbf{Závěr}:
Rovnice \eqref{eq:priklad1} má řešení $\Theta(n \log n)$.
\subsubsection*{Iterační metoda}
\begin{itemize}
	\item Rekurentní rovnici expandujeme její iterací, vedoucí na rozvoj konečné řady.
	\item Řešením je součet vzniklé řady.

\end{itemize}
Také tuto metodu lze použít pro určení horní i spodní meze řešení.
Příklad: Uvažujme rovnici
\begin{equation*}
t(n) = 3t(\lfloor n/4 \rfloor ) + n
\end{equation*}
Protože platí $\lfloor \lfloor n/4 \rfloor /4\rfloor = \lfloor n/4^2\rfloor$ atd, postupnou iterací dostaneme.
\begin{align*}
t(n) &= n + 3t(\lfloor n/4\rfloor)  \\
&= n + 3 \lfloor n/4\rfloor + 3^2t(\lfloor n/4^2\rfloor)  \\
&= n + 3 \lfloor n/4\rfloor + 3^2(\lfloor n/4^2\rfloor) + 3^3t(\lfloor n/4^3\rfloor)  \\
&= \dots  \\
&= n + 3 \lfloor n/4\rfloor + 3^2(\lfloor n/4^2\rfloor) + 3^3(\lfloor n/4^3\rfloor)+ \cdots 3^{\log_{4} n} \Theta(1) 
\end{align*}
Po zanedbání zaokrouhlovacích chyb a doplněním na nekonečnou konvergentní geometrickou řadu dostaneme

\begin{equation*}
t(n) \leq n \sum_{i=0}^{\infty} \left( \frac{3}{4}\right)^i = 4n.
\end{equation*}
Obecně je třeba
\begin{itemize}
	\item určit počet iterací, nutných pro dosažení koncových podmínek,
	\item sečíst nebo odhadnout součet konečné řady.
\end{itemize}
\subsubsection*{Mistrovská metoda}
\subsection{Řešení rekurentních rovnice}
Postup při řešení homogenní lineární rekurentní rovnice s konstatním koeficientem (HLRRsKK):
\begin{itemize}
	\item Určíme charakteristický polynom $p(\lambda)$ a jeho kořeny.
	\item Pro každé reálné charakteristické číslo $\lambda$ přidáme do báze řešení $B$ posloupnost $\{\lambda^n\}$.

	\item Je-li toto $\lambda$ násobnosti $m > 1$, pak přidáme také posloupnosti $\{n.\lambda^n\}, \{n^2 \lambda^n\}, \dots , \{n^{m-1} \lambda^n\}$
	
\end{itemize}
\begin{definition}
	Nechť $a \geq 1$ a $b > 1$ jsou konstanty, $f(n)$ funkce jedné proměnné. Uvažujme rekurentní rovnici
	\begin{equation*}
	t(n) = at(n/b) + f(n), 
	\end{equation*}
	kde $n/b$ může znamenat buď $\lfloor n/b \rfloor$ nebo $\lceil n/b \rfloor$ a tyto rozdíly zanedbáváme. Pak $t(n)$ má následující asymptotické řešení:

	\begin{itemize}
		\item[(1)] Pokud $f(n) = O(n^{\log_b a-\varepsilon})$ pro nějakou konstantu $\varepsilon > 0$, pak $t(n) = \Theta(n^{\log_b a})$.
		\item[(2)] Pokud $f(n) = \Theta(n^{\log_b a})$, pak $t(n) = \Theta(n^{\log_b a} \log n)$.
		\item[(3)] Pokud $f(n) = \Omega(n^{log_b a+\varepsilon})$ pro nějakou konstantu $\varepsilon > 0$ a pokud $af(n/b) \leq cf(n)$ pro nějakou konstantu $c < 1$ a všechna $n \geq n_0$, pak $t(n) = \Theta(f(n))$.
	\end{itemize}
\end{definition}
\subsection{Analýza časové složitost algoritmů}
Přesná složitost lze určit jen u jednodušších algoritmů. Obvykle však stačí řád růstu.
\begin{itemize}
	\item $O$-notace
		\subitem \textbf{Horní mez}
		\begin{itemize}
			\item Pro odhady v nejhorším případě.
			\item Jsou-li dány funkce $f(n)$ a $g(n)$, pak řekneme, že $f(n)$ je \textbf{nejvýše řádu} $g(n)$, psáno $f(n) = O(g(n))$, jestliže
			$\exists c \in R^+, \exists n_0 \in N^+, \forall n \geq n_0 : f(n) \leq c \cdot g(n).$
			
		\end{itemize}
	\item $\Omega$-notace
		\subitem \textbf{Dolní mez}
		\begin{itemize}
			\item Pro odhady v nejlepším případě.
			\item Jsou-li dány funkce $f(n)$ a $g(n)$, pak řekneme, že $f(n)$ je \textbf{nejméně řádu} $g(n)$, psáno $f(n) = O(g(n))$, jestliže
			$\exists c \in R^+, \exists n_0 \in N^+, \forall n \geq n_0 : c \cdot g(n) \leq f(n). $
		\end{itemize}
	\item $\Theta$-notace
		\subitem \textbf{Těsná mez}
		\begin{itemize}
			\item $\Theta$-notaci používáme pro vyjádření faktu, že dvě funkce rostou asymptoticky stejně až na multiplikativní konstantu.

			\item Jsou-li dány funkce $f(n)$ a $g(n)$, pak řekneme, že $f(n)$ je\textbf{ téhož řádu} jako $g(n)$, psáno $f(n) = O(g(n))$, jestliže
			$\exists c_1,c_2 \in R^+, \exists n_0 \in N^+, \forall n \geq n_0 : c_1 \cdot g(n) \leq f(n) \leq c_2 \cdot g(n). $
		\end{itemize}
\end{itemize}
Vztahy:
\begin{align*}
f(n) &= \Theta(g(n)) \implies f(n) = O(g(n)) \\
f(n) &= \Theta(g(n)) \implies f(n) = \Omega(g(n)) \\
f(n) &= \Theta(g(n)) \Leftrightarrow f(n) = O(g(n))  \wedge f(n) = \Omega(g(n)) \\
\end{align*}
\subsubsection*{Fibonacci}
Fibonacciho posloupnost je definována rekurentní rovnicí
\begin{equation*}
F_{n+2} - F_{n+1} - F_n = 0 \text{ pro }n \geq 1.
\end{equation*}