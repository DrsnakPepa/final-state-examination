\section{Základy integrálního počtu (primitivní funkce, neurčitý integrál, Riemannův integrál (definice, vlastnosti a geometrický význam)).}
\begin{definition}[Primitivní funkce]
\hfill

Nechť funkce $f$ je definována na intervalu $(a,b)$, kde $-\infty \le a < b \le +\infty$. Funkci $F$ splňující podmínku
\begin{align*}
	F'(x) = f(x) \textrm{ pro každé } x \in (a,b)
\end{align*}
nazýváme \textbf{primitivní funkcí} k funkci $f$ na intervalu $(a,b)$.
\end{definition}

\begin{definition}[Neurčitý integrál]
\hfill

Nechť k funkci $f$ existuje primitivní funkce na intervalu $(a,b)$. Množinu všech primitivních funkcí k funkci $f$ na $(a,b)$ nazýváme \textbf{neurčitým integrálem} a značíme jej $\int f$ nebo $\int f(x)\,dx$.
\end{definition}

\begin{note}[Terminologie]
\hfill

Najdeme-li k $f$ primitivní funkci $F$ v intervalu $(a,b)$, zapisujeme tento fakt obvykle
\begin{align*}
	\int f(x)\,dx = F(x) + c.
\end{align*}
Funkci $f$ nazýváme \textbf{integrovanou funkcí}, \textbf{$x$ integrační proměnnou} a \textbf{$c$ integrační konstantou}. Úkolu určit $\int f(x)\,dx$ říkáme "najít primitivní funkci k $f$", nebo "vypočítat integrál z $f$", nebo "integrovat $f$".
\end{note}

\begin{lemma}[Postačující podmínka pro existenci primitivní funkce]
\hfill

Nechť funkce $f$ je spojitá na intervalu $(a,b)$. Pak funkce $f$ má na tomto intervalu primitivní funkci.
\end{lemma}

\begin{definition}[Infimum]
\hfill

Buď $A$ neprázdná zdola omezená podmnožina množiny reálných čísel. Číslo $\alpha \in \mathbb{R}$ nazveme \textbf{infimem} množiny $A$, značíme $inf\ A$, právě když
\begin{itemize}
\item pro každé $x\in A$ platí $\alpha \le x$ ($\alpha$ je \textbf{dolní závora $A$}),
\item pokud $\beta \in \mathbb{R}$ také splňuje předchozí bod, pak $\beta \le \alpha$ ($\alpha$ je největší dolní závora $A$).
\end{itemize}
Pokud množina $A$ není zdola omezená, pak klademe $inf\ A := -\infty$. Pro prázdnou množinu klademe $inf\ \emptyset := +\infty$.
\end{definition}

\begin{definition}
\hfill

Buď dán interval $\langle a, b\rangle$. Konečnou množinu
\begin{align*}
	\sigma = \{ x_0, x_1, \cdots, x_n \}
\end{align*} 
takovou, že
\begin{align*}
	a = x_0 < x_1 < \cdots < x_n = b
\end{align*}
nazýváme \textbf{dělením intervalu $\langle a,b\rangle$}. Bodům $x_k, k = 1,2,\cdots, n-1$, říkáme \textbf{dělící body intervalu $\langle a, b\rangle$}. Intervalu $\langle x_{k-1},x_k\rangle$ říkáme \textbf{$k$-tý částečný interval} intervalu $\langle a,b\rangle$ při dělení $\sigma$. Číslo
\begin{align*}
	\nu (\sigma) := max\{ \Delta_k | k = 1,2,\cdots,n \}, \quad \textrm{ kde } \quad \Delta_k := x_k - x_{k-1},k = 1,2,\cdots, n,
\end{align*}
nazýváme \textbf{normou dělení $\sigma$}.
\end{definition}

\begin{definition}[Horní (dolní) součet]
\hfill

Buďte funkce $f$ definovaná a omezená na intervalu $J = \langle a,b\rangle$ a $\sigma = \{ x_0, x_1, \cdots, x_n \}$ dělení intervalu $J$. Označme
\begin{align*}
	M_i(\sigma, f) := sup_{\langle x_{i-1}, x_i\rangle}f \quad \textrm{ a } \quad m_i(\sigma, f) := inf_{\langle x_{i-1}, x_i\rangle} f
\end{align*}
pro každé $i = 1,2,\cdots,n$.
Potom součty
\begin{align*}
	S(\sigma, f) := \sum_{i=1}^{n}M_i(\sigma, f)\Delta_i \quad \textrm{ a } \quad s(\sigma, f) := \sum_{i=1}^{n}m_i(\sigma, f)\Delta_i
\end{align*}
nazýváme \textbf{horním}, resp. \textbf{dolním}, \textbf{součtem funkce $f$} při dělení $\sigma$.
\end{definition}

\begin{definition}[Horní (dolní) integrál]
\hfill

Pro funkci $f$ definovanou a omezenou na uzavřeném intervalu $J = \langle a, b\rangle$ definujeme čísla
\begin{align*}
	\overline{\int_{a}^{b}}f(x)\,dx := inf\{S(\sigma)|\sigma \textrm{ dělení } J\} \quad \textrm{ a } \quad \underline{\int_{a}^{b}}f(x)\,dx := sup\{S(\sigma)|\sigma \textrm{ dělení } J\}
\end{align*}
a nazýváme \textbf{horním}, resp. \textbf{dolním}, \textbf{integrálem} funkce $f$ na intervalu $J$.
\end{definition}

\begin{definition}[Riemannův integrál]
\hfill

Pokud pro funkci $f$ definovanou a omezenou na uzavřeném intervalu $J$ platí
\begin{align*}
\overline{\int_{a}^{b}}f(x)\,dx = \underline{\int_{a}^{b}}f(x)\,dx \in \mathbb{R},
\end{align*}
pak jejich společnou hodnotu nazýváme \textbf{Riemannovým integrálem funkce $f$ na intervalu $J$} a toto číslo značíme symboly
\begin{align*}
	\int_{a}^{b}f, \quad \textrm{ případně } \quad \int_{a}^{b}f(x)\,dx.
\end{align*}
\end{definition}

\begin{lemma}[Aditivita integrálu]
\hfill

Nechť $f$ a $g$ jsou spojité funkce na intervalu $\langle a,b\rangle$. Potom pro Riemannův integrál funkce $f+g$ (která je automaticky spojitá na $\langle a,b\rangle$), platí
\begin{align*}
	\int_{a}^{b}(f+g)(x)\,dx = \int_{a}^{b}f(x)\,dx + \int_{a}^{b}g(x)\,dx
\end{align*}
\end{lemma}

\begin{lemma}[Multiplikativita integrálu]
\hfill

Nechť $f$ je spojitá na intervalu $\langle a,b\rangle$ a $c\in \mathbb{R}$ je konstanta. Potom pro Riemannův integrál funkce $cf$ platí
\begin{align*}
	\int_{a}^{b}(cf)(x)\,dx = c\int_{a}^{b}f(x)\,dx.
\end{align*}
\end{lemma}

\begin{lemma}[Aditivita integrálu v mezích]
\hfill

Riemannův integrál funkce $f$ na intervalu $\langle a,b\rangle$ existuje, právě když pro každé $c\in (a,b)$ existují Riemannovy integrály funkce $f$ na intervalech $\langle a,c\rangle$ a $\langle c, b\rangle$. V takovém případě navíc platí
\begin{align*}
	\int_{a}^{b}f(x)\,dx = \int_{a}^{c}f(x)\,dx + \int_{c}^{b}f(x)\,dx.
\end{align*}
\end{lemma}

\begin{lemma}[Nerovnosti mezi integrály]
\hfill

Nechť jsou $f$ a $g$ spojité funkce na intervalu $\langle a, b\rangle$ a nechť platí nerovnost $f(x) \le g(x)$ pro všechna $x\in \langle a,b\rangle$. Potom pro jejich Riemannovy integrály platí
\begin{align*}
	\int_{a}^{b}f(x)\,dx \le \int_{a}^{b}g(x)\,dx.
\end{align*}
\end{lemma}

\begin{lemma}[Newtonova formule]
\hfill

Nechť $f$ je funkce spojitá na intervalu $\langle a,b\rangle$ s primitivní funkcí $F$. Pak platí rovnost
\begin{align*}
	\int_{a}^{b}f(x)\,dx = F(b) - F(a) =: \left[ F(x) \right]_{a}^{b}.
\end{align*}
\end{lemma}