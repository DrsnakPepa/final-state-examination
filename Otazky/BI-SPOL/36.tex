\section{Číselné řady (konvergence číselné řady, kritéria konvergence, odhadování rychlosti růstu řad pomocí určitého integrálu).}
\begin{definition}[Číselná řada]
	\hfill
	
	Formální výraz tvaru
	\begin{align*}
		\sum_{k=0}^{\infty}a_k = a_0+a_1+a_2+\cdots
	\end{align*}
	nazýváme \textbf{číselnou řadou}. Pokud je posloupnost \textbf{částečných součtů}
	\begin{align*}
		s_n := \sum_{k=0}^{n}a_k
	\end{align*}
	konvergentní, nazýváme příslušnou řadu také \textbf{konvergentní}. V opačném případě mluvíme o \textbf{divergentní} číselné řadě. \textbf{Součtem} konvergentní číselné řady $\sum_{k=0}^{\infty}a_k$ nazýváme hodnotu limity $\lim_{n\to +\infty}s_n$.
\end{definition}

\begin{lemma}[Nutná podmínka konvergence]
	\hfill
	
	Pokud řada $\sum_{k=0}^{\infty}a_k$ konverguje, potom pro limitu sčítanců platí $\lim_{k\to +\infty}a_k = 0$.
\end{lemma}

\begin{lemma}[Bolzano-Cauchy]
	\hfill
	
	Řada $\sum_{k=0}^{\infty}a_k$ konverguje právě tehdy, když pro každé $\varepsilon > 0$ existuje $n_0 \in \mathbb{R}$ tak, že pro každé $n \ge n_0$ a $p \in \mathbb{N}$ platí
	\begin{align*}
		|a_n + a_{n+1} + \cdots + a_{n+p}| < \varepsilon.
	\end{align*}
\end{lemma}

\begin{definition}[Absolutní konvergence]
	\hfill
	
	Řadu $\sum_{k=0}^{\infty}a_k$ nazýváme \textbf{absolutně konvergentní}, pokud řada $\sum_{k=0}^{\infty}|a_k|$ konverguje.
\end{definition}

\begin{lemma}
	\hfill
	
	Pokud řada absolutně konverguje, potom konverguje.
\end{lemma}

\subsection{Postačující podmínky konvergence}
\begin{lemma}[Leibnizovo kritérium]
	\hfill
	
	Buď $(a_n)$ monotónní posloupnost kladných členů konvergující k nule. Potom řada
	\begin{align*}
		\sum_{k=1}^{\infty}(-1)^{k}a_k
	\end{align*}
	konverguje.
\end{lemma}

\begin{lemma}[Srovnávací kritérium]
	\hfill
	
	Buďte $\sum_{k=1}^{\infty}a_k$ a $\sum_{k=1}^{\infty}b_k$ číselné řady. Potom platí následující dvě tvrzení.
	\begin{itemize}
		\item Nechť pro každé $k \in \mathbb{N}$ platí odhad $0 \le |a_k| \le b_k$ a nechť řada $\sum_{k=0}^{\infty}b_k$ konverguje. Potom řada $\sum_{k=0}^{\infty}a_k$ absolutně konverguje.
		\item Nechť pro každé $k \in \mathbb{N}$ platí odhad $0 \le a_k \le b_k$ a nechť řada $\sum_{k=0}^{\infty}a_k$ diverguje. Potom i řada $\sum_{k=0}^{\infty}b_k$ diverguje.
	\end{itemize}
\end{lemma}

\begin{lemma}[d'Alambertovo kritérium]
	\hfill
	
	Buďte $a_k > 0, k \in \mathbb{N}$. Pokud pro všechna $k \in \mathbb{N}$ od jistého $k_0 \in \mathbb{N}$ platí odhad
	\begin{align*}
		\frac{a_{k+1}}{a_k} \le q < 1,
	\end{align*}
	pak řada $\sum_{k=0}^{\infty}a_k$ (absolutně) konverguje.
\end{lemma}

\begin{itemize}
\item Ke splnění podmínky d'Alambertova kritéria například stačí, aby 
\begin{align*}
	\lim_{k\to +\infty}\frac{a_{k+1}}{a_k} = q < 1
\end{align*}
\item Podobným způsobem můžeme dále nahlédnout, že pokud všechna $a_k$ jsou kladná a 
\begin{align*}
	\frac{a_{k+1}}{a_k} \ge q > 1
\end{align*}
pro $k \ge k_0$, potom řada
\begin{align*}
	\sum_{k=0}^{\infty}a_k
\end{align*}
diverguje. V tomto případě totiž platí $\lim a_k = +\infty$ a není splněná nutná podmínka konvergence této řady.
\end{itemize}

\subsection{Odhadování rychlosti růstu různých součtů}
\begin{lemma}
	\hfill
	
	Nechť $f$ je spojitá funkce na $\langle 1, +\infty)$ a $n \in \mathbb{N}$.
	\begin{itemize}
		\item Je-li $f$ klesající, pak
		\begin{align*}
			f(n) + \int_{1}^{n} f(x)\,dx \le \sum_{k=1}^{n}f(k) \le f(1) + \int_{1}^{n} f(x)\,dx
		\end{align*}
		\item Je-li $f$ rostoucí, pak
		\begin{align*}
			f(1) + \int_{1}^{n} f(x)\,dx \le \sum_{k=1}^{n}f(k) \le f(n) + \int_{1}^{n} f(x)\,dx
		\end{align*}
	\end{itemize}
\end{lemma}

\begin{lemma}
	\hfill
	
	Buď $\sum_{n=1}^{\infty}a_n$ číselná řada s kladnými členy. Nechť existuje spojitá a monotónní funkce definovaná na $\langle 0, +\infty)$ taková, že $f(n) = a_n$ pro každé $n$. Potom:
	\begin{itemize}
		\item Pokud integrál $\int_{1}^{\infty}f(x)\,dx$ konverguje, pak číselná řada $\sum_{n=1}^{\infty}a_n$ konverguje.
		\item Pokud integrál $\int_{1}^{\infty}f(x)\,dx$ diverguje, pak číselná řada $\sum_{n=1}^{\infty}a_n$ diverguje.
	\end{itemize}
\end{lemma}