\section{Limita a derivace funkce (definice a vlastnosti, geometrický význam), využití při vyšetřování průběhu funkce.}
\subsection{Limita a derivace funkce}
\begin{definition}[Posloupnost]
	\hfill
	
	Zobrazení množiny $\mathbb{N}$ do $\mathbb{R}$ nazýváme \textbf{reálná posloupnost}.
\end{definition}

\begin{note}[Terminologie]
	\hfill
	
	\begin{itemize}
		\item Je-li $a: \mathbb{N} \rightarrow \mathbb{R}$ posloupnost, pak funkční hodnotu $a$ v bodě $n \in D_a = \mathbb{N}$, tj. $a(n)$, označujeme $a_n$ a nazýváme $n$\textbf{-tým členem posloupnosti} $a$. O $n$ v samotném kontextu mluvíme jako o indexu.
		\item Skutečnost, že $a: \mathbb{N} \rightarrow \mathbb{R}$ je posloupnost, zapisujeme také zkráceně sybolem $(a_n)$.
	\end{itemize}
\end{note}

\begin{definition}[Limita posloupnosti]
	\hfill
	
	Řekneme, že reálná posloupnost $(a_n)$ má \textbf{limitu} $\alpha \in \overline{\mathbb{R}}$, právě když pro každé okolí bodu $H_{\alpha}$ bodu $\alpha$ lze nalézt $n_0$ takové, že pro všechna $n \in \mathbb{N}$ větší než $n_0$ platí $a_n \in H_{\alpha}$. V symbolech
	\begin{align*}
		(\forall H_{\alpha})(\exists n_0 \in \mathbb{N})(\forall n \in \mathbb{N})(n > n_0 \implies a_n \in H_{\alpha}).
	\end{align*}
	Tuto skutečnost zapisujeme několika možnými způsoby:
	\begin{align*}
		\lim_{n\to\infty}a_n = \alpha \quad\textrm{nebo}\quad \lim a_n = \alpha \quad\textrm{nebo}\quad a_n \to \alpha.
	\end{align*}
\end{definition}

\begin{definition}[Okolí bodu]
	\hfill
	
	Nechť $a \in \mathbb{R}$ a $\varepsilon > 0$. Otevřený interval $(a - \varepsilon, a + \varepsilon)$ nazýváme $\varepsilon$\textbf{-okolím bodu $a$ v $\mathbb{R}$} a značíme ho $H_a(\varepsilon)$.
\end{definition}

\begin{note}
	\hfill
	
	Je-li $a \in \mathbb{R}$ a $\varepsilon > 0$, pak $x \in H_a(\varepsilon)$, právě když platí nerovnost $|x-a|<\varepsilon$. Tedy
	\begin{align*}
		H_a(\varepsilon) = \{ x \in \mathbb{R} | |x-a| < \varepsilon \}.
	\end{align*}
\end{note}

\begin{definition}[Levé (pravé) okolí bodu]
	\hfill
	
	Nechť $a \in \mathbb{R}$ a $\varepsilon > 0$. Polouzavřený interval $\langle a, a + \varepsilon)$, resp. $(a - \varepsilon, a\rangle$, nazýváme \textbf{pravým}, resp. \textbf{levým}, \textbf{$\varepsilon$-okolím bodu $a$ v $\mathbb{R}$} a značíme $H_a^+(\varepsilon)$, resp. $H_a^-(\varepsilon)$.
\end{definition}

\begin{note}
	\hfill
	
	O množině $H_a^\pm(\varepsilon)$ někdy též mluvíme jako o \textbf{jednostranném} okolí, a o $H_a(\varepsilon)$ jako o \textbf{oboustranném} okolí bodu $a \in \mathbb{R}$.
\end{note}

\begin{definition}[Limita funkce]
	\hfill
	
	Buďte $f$ reálná funkce reálné proměnné a $a \in \overline{\mathbb{R}}$. Nechť $f$ je definována na okolí bodu $a$, s možnou výjimkou bodu $a$ samotného. Řekneme, že $c \in \overline{\mathbb{R}}$ je \textbf{limitou funkce $f$ v bodě $a$}, právě když pro každé okolí $H_c$ bodu $c$ existuje okolí $H_a$ bodu $a$ takové, že z podmínky
	\begin{align*}
		x \in H_a \setminus \{a\}
	\end{align*}
	plyne
	\begin{align*}
		f(x) \in H_c.
	\end{align*}
	V symbolech:
	\begin{align*}
		(\forall H_c)(\exists H_a)(\forall x \in D_f)(x \in H_a \setminus \{a\} \implies f(x) \in H_c).
	\end{align*}
	Zapisujeme
	\begin{align*}
		\lim_{x\to a}f(x) = c \quad \textrm{nebo} \quad \lim_{a}f=c.
	\end{align*}
\end{definition}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{figs/limita_funkce}
	\caption{Limita funkce}
\end{figure}

\begin{definition}[Limita funkce zleva (zprava)]
	\hfill
	
	Buďte $f$ reálná funkce reálné proměnné a $a \in \mathbb{R}$. Nechť $f$ je definována na levém, resp. pravém, okolí bodu $a$, s možnou výjimkou bodu $a$ samotného. Řekneme, že $c \in \overline{\mathbb{R}}$ je \textbf{limitou funkce $f$ v bodě $a$ zleva, resp. zprava}, právě když pro každé okolí $H_c$ bodu $c$ existuje levé okolí $H_a^-$, resp. pravé okolí $H_a^+$, bodu $a$ takové, že z podmínky
	\begin{align*}
		x \in H_a^- \setminus \{a\}, \textrm{ resp. }  x \in H_a^+ \setminus \{a\}
	\end{align*}
	plyne
	\begin{align*}
		f(x) \in H_c.
	\end{align*}
	Zapisujeme
	\begin{align*}
		\lim_{x\to a-}f(x) = c \quad \textrm{nebo} \quad \lim_{a-}f=c,
	\end{align*}
	resp.
	Zapisujeme
	\begin{align*}
		\lim_{x\to a+}f(x) = c \quad \textrm{nebo} \quad \lim_{a+}f=c.
	\end{align*}
\end{definition}

\begin{lemma}
	\hfill
	
	Nechť $a \in \mathbb{R}$. Limita $\lim_{x\to a} f(x)$ existuje a je rovna $c \in \overline{\mathbb{R}}$, právě když existují obě jednostranné limity $\lim_{x\to a+}f(x)$ a $\lim_{x\to a-}f(x)$ a obě jsou rovny $c$.
	
	\textbf{Důsledek:}
	
	Nechť $f$ je funkce a bod $a \in \mathbb{R}$. Platí-li alespoň jedna z podmínek
	\begin{itemize}
		\item obě jednostranné limity funkce $f$ v bodě $a$ existují a jsou různé,
		\item alespoň jedna z jednostranných limit funkce $f$ v bodě $a$ neexistuje,
	\end{itemize}
	potom limita funkce $f$ v bodě $a$ neexistuje.
\end{lemma}

\begin{lemma}[Heine]
	\hfill
	
	$\lim_{x\to a} f(x) = c$, právě když je $f$ definována na okolí bodu $a$ (s možnou výjimkou bodu $a$ samotného) a pro každou posloupnost $(x_n)$ s limitou $a$ splňující $\{ x_n | n \in \mathbb{N} \} \subset D_f \setminus \{a\}$ platí $\lim_{n\to\infty}f(x_n) = c$.
\end{lemma}

\begin{lemma}[Heine pro jednostranné limity]
	\hfill
	
	$\lim_{x\to a-} f(x) = c$, resp. $\lim_{x\to a+} f(x) = c$, právě když $f$ je definována na levém, resp. pravém, okolí bodu $a$ a pro každou posloupnost $(x_n)$ s limitou $a$ splňující $\{ x_n | n \in \mathbb{N} \} \subset D_f \cap (-\infty, a)$, resp. $\{ x_n | n \in \mathbb{N} \} \subset D_f \cap (a, +\infty)$, platí $\lim_{n\to\infty}f(x_n) = c$.
\end{lemma}

\begin{lemma}
	\hfill
	
	Nechť $f$ a $g$ jsou funkce a $a$ prvek z $\overline{\mathbb{R}}$. Potom
	\begin{align*}
		\lim_{a}(f+g) &= \lim_{a} f + \lim_{a} g,\\
		\lim_{a}(f\cdot g) &= \lim_{a}f \cdot \lim_{a}g,\\
		\lim_{a}\frac{f}{g} &= \frac{\lim_{a}f}{\lim_{a}g}
	\end{align*}
	platí v případě, že výrazy na pravé straně jsou definovány a v posledním případě za předpokladu, že $\frac{f}{g}$ je definována na okolí bodu $a$ s možnou výjimkou bodu $a$ samotného.
\end{lemma}

\begin{lemma}[O limitě složené funkce]
	\hfill
	
	Nechť $f$ a $g$ jsou funkce, $a$,$b$,$c$ jsou prvky z $\overline{\mathbb{R}}$ a platí tři podmínky
	\begin{itemize}
		\item $\lim_{x\to b}f(x) = c$,
		\item $\lim_{x\to a}g(x) = b$,
		\item buď $(\exists H_a)(\forall x \in D_g \cap H_a \setminus \{a\})(g(x) \ne b)$ nebo $(b \in D_f a f(b) = c)$.
	\end{itemize}
	Potom $\lim_{x\to a}f(g(x)) = c$.
\end{lemma}

\begin{definition}[Derivace funkce v bodě]
	\hfill
	
	Nechť $f$ je funkce definovaná na okolí bodu $a \in \mathbb{R}$. Pokud existuje limita
	\begin{align*}
		\lim_{x\to a}\frac{f(x)-f(a)}{x-a},
	\end{align*}
	nazveme její hodnotu \textbf{derivací funkce $f$ v bodě $a$} a označíme $f'(a)$. Pokud je tato konečná (tj. $f'(a) \in \mathbb{R}$), řekneme, že \textbf{funkce $f$ je diferencovatelná v bodě $a$}.
\end{definition}

\begin{definition}[Derivace funkce]
	\hfill
	
	Buď $f$ funkce s definičním oborem $D_f$.  Nechť $M$ označuje množinu všech $x \in D_f$ takových, že existuje konečná derivace $f'(x)$. Derivací funkce $f$ nazýváme funkci s definičním oborem $M$, která každému $x \in M$ přiřadí $f'(x)$. Tuto funkci značíme symbolem $f'$.
\end{definition}

\subsection{Vyšetřování průběhu funkce}
\begin{definition}[Spojitost funkce]
	\hfill
	
	Nechť $f$ je reálná funkce reálné proměnné a nechť bod $a \in D_f$. Řekneme, že funkce $f$ je \textbf{spojitá v bodě $a$}, jestliže nastává alespoň jedna z následujících možností:
	\begin{itemize}
		\item $\lim_{x\to a}f(x) = f(a)$,
		\item funkce $f$ je spojitá na pravém okolí bodu $a$, přesněji $(\exists H_a)(H_a \cap D_f = H_a^+)$, a $\lim_{x\to a+}f(x) = f(a)$,
		\item funkce $f$ je spojitá na levém okolí bodu $a$, přesněji $(\exists H_a)(H_a \cap D_f = H_a^-)$, a $\lim_{x\to a-}f(x) = f(a)$.
	\end{itemize}
	Funkce $f$ je \textbf{spojitá v bodě $a$ zprava}, pokud $\lim_{x\to a+}f(x) = f(a)$.
	Funkce $f$ je \textbf{spojitá v bodě $a$ zleva}, pokud $\lim_{x\to a-}f(x) = f(a)$.
\end{definition}

\begin{definition}[Tečna funkce]
	\hfill
	
	Nechť existuje $f'(a)$. Je-li
	\begin{itemize}
		\item funkce $f$ spojitá v bodě $a$ a $f'(a) = +\infty$ nebo $f'(a) = -\infty$, nazýváme přímku s rovnicí $x=a$
		\item $f'(a) \in \mathbb{R}$, nazýváme přímku s rovnicí $y = f(a) + f'(a)(x-a)$
	\end{itemize}
	\textbf{tečnou funkce $f$ v bodě $a$}.
\end{definition}

\begin{lemma}
	\hfill
	
	Je-li funkce $f$ diferencovatelná v bodě $a$, pak je spojitá v bodě $a$. Tj. platí
	\begin{align*}
		f'(a) \in \mathbb{R} \implies \lim_{x\to a}f(x) = f(a).
	\end{align*}
\end{lemma}

\begin{lemma}[Derivace součtu, součinu a podílu]
	\hfill
	
	Nechť funkce $f$ a $g$ jsou diferencovatelné v bodě $a$. Potom platí
	\begin{itemize}
		\item $(f+g)'(a) = f'(a) + g'(a)$,
		\item $(f\cdot g)'(a) = f'(a)g(a)+f(a)g'(a)$,
		\item $\left(\frac{f}{g}\right)'(a) = \frac{f'(a)g(a) - f(a)g'(a)}{g(a)^2}$, pokud $g(a) \ne 0$.
	\end{itemize}
\end{lemma}

\begin{lemma}[Derivace složené funkce]
	\hfill
	
	Nechť $g$ je funkce diferencovatelná v bodě $a$, $f$ je diferencovatelná v bodě $g(a)$. Potom funkce $f\circ g$ je diferencovatelná v bodě $a$ a platí
	\begin{align*}
		(f\circ g)'(a) = f'(g(a))\cdot g'(a).
	\end{align*}
\end{lemma}

\begin{lemma}[Derivace inverzní funkce]
	\hfill
	
	Buďte $f$ spojitá a monotónní na intervalu $I = (a,b)$ a bod $c\in I$. Má-li inverzní funkce $f^{-1}$ konečnou nenulovou derivaci v bodě $f(c)$, potom má $f$ derivaci v bodě $c$ a platí
	\begin{align*}
		f'(c) = \frac{1}{(f^{-1})'(f(c))}.
	\end{align*}
\end{lemma}

\begin{definition}[Extrémy funkce]
	\hfill
	
	Řekneme, že funkce $f$ má v bodě $a\in D_f$
	\begin{itemize}
		\item \textbf{lokální maximum}
		\item \textbf{lokální minimum}
		\item \textbf{ostré lokální maximum}
		\item \textbf{ostré lokální minimum}
	\end{itemize}
	právě když existuje okolí (v krajním bodě jednostranné) $H_a \subset D_f$ bodu $a$ tak, že
	\begin{itemize}
		\item pro všechna $x \in H_a$ platí $f(x) \le f(a)$,
		\item pro všechna $x \in H_a$ platí $f(x) \ge f(a)$,
		\item pro všechna $x \in H_a \setminus \{a\}$ platí $f(x) < f(a)$,
		\item pro všechna $x \in H_a \setminus \{a\}$ platí $f(x) > f(a)$.
	\end{itemize}
\end{definition}

\begin{lemma}[Nutná podmínka existence lokální extrému]
	\hfill
	
	Nechť funkce $f$ má v bodě $a$ lokální extrém. Potom $f'(a) = 0$, nebo derivace v bodě $a$ neexistuje.
\end{lemma}

\begin{lemma}[Extrém spojité funkce na uzavřeném intervalu]
	\hfill
	
	Funkce $f$ spojitá na uzavřeném intervalu $\langle a,b\rangle$ nabývá maxima a minima (tzv. \textbf{globální extrém}). Extrém může být pouze v krajních bodech $a$, $b$ a v bodech, kde je derivace rovna $0$ nebo neexistuje.
\end{lemma}

\begin{lemma}[Rolleova]
	\hfill
	
	Nechť funkce $f$ splňuje podmínky
	\begin{itemize}
		\item $f$ je spojitá na intervalu $\langle a, b\rangle$,
		\item $f$ má derivace v každém bodě intervalu $(a, b)$,
		\item $f(a) = f(b)$.
	\end{itemize}
	Potom existuje $c \in (a,b)$ tak, že $f'(c) = 0$.
\end{lemma}

\begin{lemma}[Lagrangeova, O přírůstku funkce]
	\hfill
	
	Nechť funkce $f$ splňuje podmínky
	\begin{itemize}
		\item $f$ je spojitá na intervalu $\langle a, b\rangle$,
		\item $f$ má derivaci v každém bodě intervalu $(a, b)$.
	\end{itemize}
	Potom existuje bod $c \in (a,b)$ tak, že $f'(c) = \frac{f(b) - f(a)}{b-a}$.
\end{lemma}

\begin{definition}[Vnitřek intervalu]
	\hfill
	
	Nechť $J$ je interval s krajními body $a$ a $b$. Potom \textbf{vnitřkem intervalu} $J$ nazveme otevřený interval $(a,b)$. Značíme ho $J^{\circ} = (a,b)$.
\end{definition}

\begin{lemma}
	\hfill
	
	Nechť $f$ je spojitá na intervalu $J$ a nechť pro každé $x\in J^{\circ}$ existuje $f'(x)$. Potom platí následujících pět tvrzení
	\begin{itemize}
		\item $(\forall x \in J^{\circ})(f'(x) \ge 0) \implies\ f\textrm{ je neklesající na }J$,
		\item $(\forall x \in J^{\circ})(f'(x) \le 0) \implies\ f\textrm{ je nerostoucí na }J$,
		\item $(\forall x \in J^{\circ})(f'(x) > 0) \implies\ f\textrm{ je rostoucí na }J$,
		\item $(\forall x \in J^{\circ})(f'(x) < 0) \implies\ f\textrm{ je klesající na }J$,
		\item $(\forall x \in J^{\circ})(f'(x) = 0) \implies\ f\textrm{ je konstantní na }J$.
	\end{itemize}
\end{lemma}

\begin{definition}[Konvexnost (konkávnost) funkce na intervalu]
	\hfill
	
	Funkci $f$ definovanou na intervalu $J$ nazveme \textbf{konvexní} (resp. \textbf{konkávní}) \textbf{na intervalu $J$}, právě když pro každé $x_1, x_2, x_3 \in J$ splňující $x_1 < x_2 < x_3$, leží bod $(x_2, f(x_2))$ buďto pod (resp. nad) přímkou spojující body $(x_1, f(x_1))$ a $(x_3, f(x_3))$, nebo na ní.
\end{definition}

\begin{definition}[Ryzí konvexnost (konkávnost) funkce na intervalu]
	\hfill
	
	Funkci $f$ definovanou na intervalu $J$ nazveme \textbf{ryze konvexní} (resp. \textbf{ryze konkávní}) \textbf{na intervalu $J$}, právě když pro každé $x_1, x_2, x_3 \in J$ splňující $x_1 < x_2 < x_3$, leží bod $(x_2, f(x_2))$ buďto pod (resp. nad) přímkou spojující body $(x_1, f(x_1))$ a $(x_3, f(x_3))$.
\end{definition}

\begin{lemma}
	\hfill
	
	Buď $f$ funkce spojitá na intervalu $J$, která má druhou derivaci v každém bodě $J^{\circ}$.
	\begin{itemize}
		\item Funkce $f$ je konvexní na intervalu $J$, právě když $f''(x) \ge 0$ pro každé $x \in J^{\circ}$.
		\item Je-li $f''(x) > 0$ v každém bodě $x\in J^{\circ}$, pak je $f$ ryze konvexní na intervalu $J$.
	\end{itemize}
\end{lemma}

\begin{definition}[Konkávnost (konvexnost) funkce v bodě]
	\hfill
	
	Nechť funkce $f$ je diferencovatelná v bodě $a \in D_f$. Pokud existuje okolí $H_a$ bodu $a$ takové, že pro všechna $x \in H_a \setminus \{a\}$ leží všechny body $(x, f(x))$ nad (resp. pod) tečnou funkce $f$ v bodě $a$,
	\begin{align*}
		y = f(a) + f'(a)(x-a),
	\end{align*}
	nebo na ní, pak $f$ nazveme \textbf{konvexní} (resp. \textbf{konkávní}) v bodě $a$.
\end{definition}

\begin{lemma}
	\hfill
	
	Buď $f$ funkce diferencovatelná v každém bodě intervalu $J$ a nechť $f'(c) = 0$ pro jisté $c\in J^{\circ}$.
	\begin{itemize}
		\item Pokud je $f$ konvexní na $J$, pak má funkce $f$ v bodě $c$ lokální mimimum.
		\item Pokud je $f$ konkávní na $J$, pak má funkce $f$ v bodě $c$ lokální maximum.
	\end{itemize}
\end{lemma}

\begin{definition}[Inflexní bod]
	Nechť $f$ je spojitá v bodě $c$. Bod $c$ nazýváme \textbf{inflexním bodem} funkce $f$, právě když existuje $\delta > 0$ takové, že $f$ je ryze konvexní na intervalu $(c-\delta, c)$ a ryze konkávní na intervalu $(c, c+\delta)$, nebo naopak.
\end{definition}

\begin{definition}[Asymptota]
	\hfill
	
	\begin{itemize}
	\item Řekneme, že funkce $f$ má v bodě $a$ \textbf{asymptotu} $x=a$, právě když $\lim_{x\to a+}f(x)$ nebo $\lim_{x\to a-}$ existuje a je rovná $+\infty$ nebo $-\infty$.
	\item Řekneme, že přímka $y = kx+q$ je \textbf{asymptotou} funkce $f$ v $+\infty$, resp. $-\infty$, když
	\begin{align*}
		\lim_{x\to \infty}(f(x) - kx - q) = 0, \textrm{ resp. } \lim_{x\to -\infty}(f(x) - kx - q) = 0.
	\end{align*}
	\end{itemize}
\end{definition}

\begin{lemma}[l'Hospitalovo pravidlo]
	\hfill
	
	Nechť pro funkce $f$ a $g$ a bod $a \in \overline{\mathbb{R}}$ platí
	\begin{itemize}
		\item $\lim_{a}f = \lim_{a}g = 0$ nebo $\lim_{a}|g| = +\infty$,
		\item existuje okolí $H_a$ bodu $a$ splňující $H_a \setminus \{a\} \subset D_{f/g} \cap D_{f'/g'}$,
		\item existuje $\lim_{a}\frac{f'}{g'}$.
	\end{itemize}
	Potom existuje $\lim_{a}\frac{f}{g}$ a platí $\lim_{a}\frac{f}{g} = \lim_{a}\frac{f'}{g'}$.
\end{lemma}

\begin{itemize}
	\item Při vyšetřování průběhu funkce zkoumáme:
	\begin{itemize}
		\item definiční obor funkce $f$, průsečíky grafu s osami, symetrie (sudost, lichost, periodicita),
		\item spojitost, body nespojitosti, existenci asymptot, limity v krajních bodech/nekonečnech,
		\item existence derivace $f'$, monotonii funkce, lokální a globální extrémy,
		\item existence druhé derivace $f''$, konvexnost, konkávnost,
		\item na základě těchto výsledků načrtneme graf funkce $f$.
	\end{itemize}
\end{itemize}